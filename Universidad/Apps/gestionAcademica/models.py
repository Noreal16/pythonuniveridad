from django.db import models

# Create your models here.

class Alumno (models.Model):

        ApellidoPaterno = models.CharField(max_length= 35)
        ApellidoMaterno = models.CharField(max_length=35)
        Nombres = models.CharField(max_length=35)
        DNI = models.CharField(max_length=10)
        FechaNacimiento = models.DateTimeField()
        Sexos = (('F', 'Femenino'), ('M','Masculino'))
        Sexo = models.CharField(max_length=1, choices=Sexos, default='M')
        #se crea una funcion
        def NombreCompleto(self):
            cadena = "{0} {1} {2}"
            return  cadena.format(self.ApellidoPaterno, self.ApellidoMaterno, self.Nombres)

          #sobre escribir el metodo toString def __str__(self): se deja un espacio
        def __str__(self):
            return self.NombreCompleto()

        #crear la case curso que hereda de la clase models
class Curso(models.Model):
        Nombre = models.CharField(max_length=50)
        #para decalrar un entero pequenio positivo PositiveSmallIntegerField()
        Creditos= models.PositiveSmallIntegerField()
        estado= models.BooleanField(default=True)

        #sobre escribimos el metodo toString
        def __str__(self):
            return "{0} ({1})".format(self.Nombre, self.Creditos)

        #se crea clase materia relacionada con creditos y alumnos
class Materia(models.Model):
        #se utiliza el foreingkey para hacer referencia con otra tabla
        #model.Cascade para hacer una eliminacion en cascada
        Alumno=models.ForeignKey(Alumno, null=False, blank=False, on_delete=models.CASCADE)
        Curso=models.ForeignKey(Curso, null=False, blank=False, on_delete=models.CASCADE)
        #DateTimeField es un atributo que almacena fechas y horas obtiene la hora del servidor
        FechaMatricula=models.DateTimeField(auto_now_add=True)
        def __str__(self):
            #spara retornar el nombre del alumno seguido al curso que esta matriculado
            cadena = "{0} => {1}"
            #self se lo utiliza para presentar
            return cadena.format(self.Alumno, self.Curso.Nombre)