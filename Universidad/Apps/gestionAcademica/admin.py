from django.contrib import admin
#para importar los datos de la universidad
from Universidad.Apps.gestionAcademica.models import *

# Register your models here.

admin.site.register(Alumno)
admin.site.register(Curso)
admin.site.register(Materia)